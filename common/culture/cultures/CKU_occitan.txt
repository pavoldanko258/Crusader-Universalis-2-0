﻿occitan_group = {
	graphical_cultures = {
		frankish_group_coa_gfx
		western_coa_gfx
		western_building_gfx
		western_clothing_gfx
		western_unit_gfx
	}
	mercenary_names = {
	}

	occitan = {
		graphical_cultures = {
			mediterranean_building_gfx
			occitan_coa_gfx
		}

		color = { 0.1 0.7 0.8 }

		cadet_dynasty_names = {
			{ "dynnp_de" "dynn_Rodez" }
			{ "dynnp_de" "dynn_Bergerac" }
			{ "dynnp_de" "dynn_L_isle_Jourdain" }
			{ "dynnp_de" "dynn_Poitiers-Valentinois" }
			"dynn_Trencval"
			{ "dynnp_de" "dynn_ChA_tellerault" }
			{ "dynnp_de" "dynn_Rancon" }
			{ "dynnp_de" "dynn_Guyenne" }
			{ "dynnp_de" "dynn_Bezieres" }
			{ "dynnp_de" "dynn_Arelat" }
			{ "dynnp_de" "dynn_Ponthieu" }
			{ "dynnp_de" "dynn_Provence" }
			{ "dynnp_de" "dynn_Toulouse" }
			{ "dynnp_d_" "dynn_Aquitaine" }
			{ "dynnp_de" "dynn_PontevE_s" }
			{ "dynnp_de" "dynn_Sabran" }
			{ "dynnp_de" "dynn_Crussol" }
			{ "dynnp_de" "dynn_Quatrebarbes" }
			{ "dynnp_de" "dynn_la_Panouse" }
			{ "dynnp_de" "dynn_LE_vezou" }
			{ "dynnp_de" "dynn_la_Rochelambert" }
			{ "dynnp_de" "dynn_Cadoine" }
			{ "dynnp_de" "dynn_Kerret" }
			{ "dynnp_de" "dynn_Donges" }
			{ "dynnp_de" "dynn_Rouvray" }
			{ "dynnp_de" "dynn_Poissy" }
			{ "dynnp_de" "dynn_Cayeux" }
			{ "dynnp_de" "dynn_Beaujeu" }
			{ "dynnp_de" "dynn_Gourdon" }
			{ "dynnp_d_" "dynn_Anduze" }
			{ "dynnp_de" "dynn_Puylaurens" }
			{ "dynnp_de" "dynn_Castelbon" }
			{ "dynnp_de" "dynn_Fenolhet" }
			{ "dynnp_de" "dynn_FontiE_s" }
			{ "dynnp_d_" "dynn_OmE_las" }
			{ "dynnp_de" "dynn_Montaner" }
			"dynn_Faucoi"
			{ "dynnp_de" "dynn_Got" }
			{ "dynnp_de" "dynn_Montcatanier" }
			{ "dynnp_de" "dynn_Caylus" }
			{ "dynnp_de" "dynn_Trians" }
			{ "dynnp_de" "dynn_Gavarret" }
			{ "dynnp_d_" "dynn_UzE_s" }
			{ "dynnp_de" "dynn_Villars" }
			{ "dynnp_de" "dynn_Rovignan" }
			{ "dynnp_de" "dynn_La_Valette" }
			{ "dynnp_d_" "dynn_Amalric" }
			{ "dynnp_le" "dynn_Bourg" }
			{ "dynnp_de" "dynn_Blanchegard" }
			{ "dynnp_de" "dynn_Villaret" }
			{ "dynnp_d_" "dynn_Aubusson" }
			{ "dynnp_de" "dynn_Montferrand" }
			{ "dynnp_de" "dynn_Poitiers" }
			{ "dynnp_de" "dynn_Bordeaux" }
			{ "dynnp_d_" "dynn_Albret" }
			{ "dynnp_de" "dynn_Labourd" }
			{ "dynnp_de" "dynn_Foix" }
			{ "dynnp_de" "dynn_RosellO_" }
			{ "dynnp_de" "dynn_Narbonne" }
			{ "dynnp_de" "dynn_Toulouse" }
			{ "dynnp_d_" "dynn_Agen" }
			{ "dynnp_de" "dynn_Rouergue" }
			{ "dynnp_de" "dynn_Provence" }
			{ "dynnp_de" "dynn_Venaissin" }
			{ "dynnp_de" "dynn_Charolais" }
			{ "dynnp_de" "dynn_Forcalquier" }
			{ "dynnp_de" "dynn_Nice" }
		}

		dynasty_names = {
			{ "dynnp_de" "dynn_Rodez" }
			{ "dynnp_de" "dynn_Bergerac" }
			{ "dynnp_de" "dynn_L_isle_Jourdain" }
			{ "dynnp_de" "dynn_Poitiers-Valentinois" }
			"dynn_Trencval"
			{ "dynnp_de" "dynn_ChA_tellerault" }
			{ "dynnp_de" "dynn_Rancon" }
			{ "dynnp_de" "dynn_Guyenne" }
			{ "dynnp_de" "dynn_Bezieres" }
			{ "dynnp_de" "dynn_Arelat" }
			{ "dynnp_de" "dynn_Ponthieu" }
			{ "dynnp_de" "dynn_Provence" }
			{ "dynnp_de" "dynn_Toulouse" }
			{ "dynnp_d_" "dynn_Aquitaine" }
			{ "dynnp_de" "dynn_PontevE_s" }
			{ "dynnp_de" "dynn_Sabran" }
			{ "dynnp_de" "dynn_Crussol" }
			{ "dynnp_de" "dynn_Quatrebarbes" }
			{ "dynnp_de" "dynn_la_Panouse" }
			{ "dynnp_de" "dynn_LE_vezou" }
			{ "dynnp_de" "dynn_la_Rochelambert" }
			{ "dynnp_de" "dynn_Cadoine" }
			{ "dynnp_de" "dynn_Kerret" }
			{ "dynnp_de" "dynn_Donges" }
			{ "dynnp_de" "dynn_Rouvray" }
			{ "dynnp_de" "dynn_Poissy" }
			{ "dynnp_de" "dynn_Cayeux" }
			{ "dynnp_de" "dynn_Beaujeu" }
			{ "dynnp_de" "dynn_Gourdon" }
			{ "dynnp_d_" "dynn_Anduze" }
			{ "dynnp_de" "dynn_Puylaurens" }
			{ "dynnp_de" "dynn_Castelbon" }
			{ "dynnp_de" "dynn_Fenolhet" }
			{ "dynnp_de" "dynn_FontiE_s" }
			{ "dynnp_d_" "dynn_OmE_las" }
			{ "dynnp_de" "dynn_Montaner" }
			"dynn_Faucoi"
			{ "dynnp_de" "dynn_Got" }
			{ "dynnp_de" "dynn_Montcatanier" }
			{ "dynnp_de" "dynn_Caylus" }
			{ "dynnp_de" "dynn_Trians" }
			{ "dynnp_de" "dynn_Gavarret" }
			{ "dynnp_d_" "dynn_UzE_s" }
			{ "dynnp_de" "dynn_Villars" }
			{ "dynnp_de" "dynn_Rovignan" }
			{ "dynnp_de" "dynn_La_Valette" }
			{ "dynnp_d_" "dynn_Amalric" }
			{ "dynnp_le" "dynn_Bourg" }
			{ "dynnp_de" "dynn_Blanchegard" }
			{ "dynnp_de" "dynn_Villaret" }
			{ "dynnp_d_" "dynn_Aubusson" }
			{ "dynnp_de" "dynn_Montferrand" }
			{ "dynnp_de" "dynn_Poitiers" }
			{ "dynnp_de" "dynn_Bordeaux" }
			{ "dynnp_d_" "dynn_Albret" }
			{ "dynnp_de" "dynn_Labourd" }
			{ "dynnp_de" "dynn_Foix" }
			{ "dynnp_de" "dynn_RosellO_" }
			{ "dynnp_de" "dynn_Narbonne" }
			{ "dynnp_de" "dynn_Toulouse" }
			{ "dynnp_d_" "dynn_Agen" }
			{ "dynnp_de" "dynn_Rouergue" }
			{ "dynnp_de" "dynn_Provence" }
			{ "dynnp_de" "dynn_Venaissin" }
			{ "dynnp_de" "dynn_Charolais" }
			{ "dynnp_de" "dynn_Forcalquier" }
			{ "dynnp_de" "dynn_Nice" }
		}

		male_names = {
			AdhE_mar Acfred Aimeric Alberic Albert Aldebert Alias AnfO_s Amalric Amaneus Ancelmes AndrE_ Archambaut Arnaut Artau Aton AudoI_n Aymar Aymeric Aznar
			Barral BartoumiE_u BaudoI_n Beneset BE_rard BerenguiE_ Bermond Bernat Bernat-Aton Bernat-Ezi Bertrand Blasi Borel Boson Carles
			ClamenC_ Centolh DA_vi Dat Dodon Doumenge Duran Ebles EmmanuE_l Enric Ermengau EstE_ve Filip
			Ferrand FlorE_nC_ Folquet FrancE_s Frederi Garcia Gaston Gausbert Gautier GilbE_rt Girard Girvais Godafres
			Gui Guichard Guigues GuilhE_m Guiraud Guitart Ives Jacme Jaufret Joan Jordan Jorge
			Josselin Julian LaurE_nC_ Leon LoI_s Loui Lop MarC_au Martin Matfre Matias Milo Miquel
			Nicholaus Odon Otton PE_ire PE_ire-Arnaut PE_ire-Raimond Peranudet Pol Pons Raimond Raimond-BerenguiE_ Rainaut Rainer
			Raolf Ricard RobE_rt Rogier Sancho Savarics Simon TiE_rri Tibaud Toumas Ubald Ubert Uc Ugues
			VE_zian VicenC_
		}
		female_names = {
			AdE_la AdalaI_da Adeltrudis Agata AI_na AinE_s AlaI_s AlienO_r AliC_ AlmO_dis AlpaI_s Ana Arsenda Assalhida Ava
			AzalaI_s Azelma BO_na Beatritz Berenguela Berta Blanca Brandimena Bregida Brunissenda Camila CarlO_ta Caterina Cecilia
			Clara Clarmonda ClemE_ncia Cloutilda ConstA_ncia DolC_a EisabE_u Elena
			Elisa ElisabE_ta Ermengarda Ermessentz Esclarmonda Estefania EufE_mia Eufrosina
			Faidida Filipa Filomena FlO_ra Francesa Garcenda Geneviva GerbE_rga Gersenda Gisla Guigone Guilheumina Heloise
			Ioulanda IsabE_u IsabE_la Joana Juliana LU_cia Laura LoI_sa Mabila Madalena
			Margarida Maria Marquisa Marta MascarO_sa Matilda MelisE_nda Navar Patricia PeironE_la
			Petronilha Puelle Raisenda Raimonda RichE_nda RosE_la Rosa SanC_a Sibilla TerE_sa Tiborg Verounica Violent
		}
		dynasty_of_location_prefix = "dynnp_de"
		grammar_transform = french
		
		#patronym_suffix_male = "dynnpat_suf_enc"
		#patronym_suffix_female = "dynnpat_suf_enc"
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 30
		mat_grf_name_chance = 10
		father_name_chance = 25
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 20
		mat_grm_name_chance = 40
		mother_name_chance = 5
		
		ethnicities = {
			10 = caucasian_blond
			5 = caucasian_ginger
			45 = caucasian_brown_hair
			35 = caucasian_dark_hair
		}


		mercenary_names = {
			{ name = "mercenary_company_routiers_of_the_archpriest" }
			{ name = "mercenary_company_host_of_the_viscount" }
			{ name = "mercenary_company_marchers_of_wasconia" }
		}
	}
	
	gascon = {
		graphical_cultures = {
			french_coa_gfx
		}
		color = { 0.1 0.4 0.7 }

		cadet_dynasty_names = {
			{ "dynnp_of" "dynn_Capet" }
			"dynn_Fournier"
			{ "dynnp_du" "dynn_Dros" }
			{ "dynnp_de" "dynn_Piemont" }
			{ "dynnp_de" "dynn_Bage" }
			{ "dynnp_de" "dynn_Montferrat" }
			{ "dynnp_de" "dynn_Luxembourg" }
			{ "dynnp_d_" "dynn_Anjou" }
			"dynn_Chamillart"
			{ "dynnp_de" "dynn_ChA_tillion" }
			{ "dynnp_d_" "dynn_Avaugour" }
			{ "dynnp_de" "dynn_Dreaux" }
			{ "dynnp_de" "dynn_Dreux" }
			{ "dynnp_de" "dynn_Clermont" }
			{ "dynnp_de" "dynn_Vitry" }
			{ "dynnp_de" "dynn_Dampierre" }
			{ "dynnp_de" "dynn_Sully" }
			{ "dynnp_de" "dynn_Bourg" }
			{ "dynnp_de" "dynn_Gironde" }
			{ "dynnp_d_" "dynn_Artois" }
			{ "dynnp_de" "dynn_Valois" }
			{ "dynnp_de" "dynn_Guines" }
			"dynn_Saint-Gilles"
			{ "dynnp_d_" "dynn_E_olienne" }
			{ "dynnp_de" "dynn_Bessa" }
			{ "dynnp_de" "dynn_Ramla" }
			{ "dynnp_d_" "dynn_Ibelin" }
			"dynn_Luneville"
			{ "dynnp_de" "dynn_Cleriuex" }
			{ "dynnp_de" "dynn_Valpergue" }
			{ "dynnp_de" "dynn_Cuiseaux" }
			{ "dynnp_de" "dynn_Joinville" }
			{ "dynnp_de" "dynn_Vaudemont" }
			{ "dynnp_d_" "dynn_Hauteville" }
			{ "dynnp_de" "dynn_Narbonne" }
			{ "dynnp_de" "dynn_Bourgogne-ComtE_" }
			{ "dynnp_de" "dynn_l_Aigle" }
			{ "dynnp_de" "dynn_Flotte" }
			{ "dynnp_de" "dynn_Vendome" }
			{ "dynnp_de" "dynn_Champagne" }
			{ "dynnp_de" "dynn_Namur" }
			{ "dynnp_de" "dynn_Bissy" }
			{ "dynnp_d_" "dynn_Eu" }
			{ "dynnp_du" "dynn_Perche" }
			{ "dynnp_de" "dynn_Boulogne" }
			"dynn_Puy_du_Fou"
			{ "dynnp_de" "dynn_Preuilly" }
			{ "dynnp_de" "dynn_Laval" }
			{ "dynnp_de" "dynn_Salins" }
			{ "dynnp_de" "dynn_ME_ziriac" }
			{ "dynnp_d_" "dynn_Oisy" }
			{ "dynnp_de" "dynn_Barthe" }
			{ "dynnp_de" "dynn_Fezensac" }
			{ "dynnp_de" "dynn_Boisrobert" }
			{ "dynnp_de" "dynn_Bourbon" }
			{ "dynnp_d_" "dynn_AngoulE_me" }
			"dynn_Le_Tellier"
			{ "dynnp_de" "dynn_Talleyrand" }
			{ "dynnp_de" "dynn_Batarnay" }
			{ "dynnp_de" "dynn_Beaumont-au-Maine" }
			{ "dynnp_de" "dynn_Hainaut" }
			{ "dynnp_de" "dynn_Crecy" }
			{ "dynnp_de" "dynn_Bellegarde" }
			{ "dynnp_de" "dynn_St__Hilary" }
			{ "dynnp_d_" "dynn_Aubigny" }
			{ "dynnp_de" "dynn_Albemarle" }
			{ "dynnp_de" "dynn_Verre" }
			{ "dynnp_d_" "dynn_Evreux" }
			{ "dynnp_de" "dynn_Mellent" }
			{ "dynnp_de" "dynn_Baalun" }
			{ "dynnp_de" "dynn_Vernon" }
			"dynn_Estouteville"
			"dynn_Nicolay"
			{ "dynnp_of" "dynn_Burgundy" }
			{ "dynnp_de" "dynn_Vexin-Amiens" }
			{ "dynnp_de" "dynn_Nimes" }
			{ "dynnp_de" "dynn_Tilly" }
			{ "dynnp_de" "dynn_Mons" }
			{ "dynnp_de" "dynn_Hainault" }
			{ "dynnp_de" "dynn_Montaigu" }
			{ "dynnp_de" "dynn_Benserade" }
			"dynn_Barrois"
			{ "dynnp_de" "dynn_Harcourt" }
			"dynn_Matfrieding"
			{ "dynnp_de" "dynn_Carolui" }
			{ "dynnp_de" "dynn_Lagery" }
			"dynn_Litpolden"
			{ "dynnp_von" "dynn_Franken" }
			{ "dynnp_de" "dynn_Maurienne" }
			{ "dynnp_de" "dynn_Vienne" }
			{ "dynnp_de" "dynn_Foix" }
			{ "dynnp_de" "dynn_Lorraine" }
			{ "dynnp_de" "dynn_Vermandois" }
			{ "dynnp_de" "dynn_Blois" }
			{ "dynnp_de" "dynn_Gastinois" }
			{ "dynnp_de" "dynn_Guincamp" }
			{ "dynnp_de" "dynn_Bretagne" }
			{ "dynnp_de" "dynn_Boullion" }
			{ "dynnp_de" "dynn_Longwy" }
			{ "dynnp_de" "dynn_Macon" }
			{ "dynnp_de" "dynn_Roucy" }
			{ "dynnp_de" "dynn_Heismes" }
			{ "dynnp_de" "dynn_SE_mur-en-Brionnais" }
			"dynn_Fergant"
			{ "dynnp_de" "dynn_Bolougne" }
			{ "dynnp_d_" "dynn_Antoing" }
			{ "dynnp_d_" "dynn_Espagne" }
			{ "dynnp_d_" "dynn_Arc" }
			"dynn_Gladius_Christi"
			{ "dynnp_de" "dynn_Mortemart" }
			{ "dynnp_de" "dynn_la_Rochefoucauld" }
			{ "dynnp_de" "dynn_Caumont" }
			{ "dynnp_de" "dynn_Serrant" }
			{ "dynnp_de" "dynn_la_Roche_Aymon" }
			{ "dynnp_de" "dynn_LE_vis" }
			{ "dynnp_de" "dynn_Villeneuve" }
			{ "dynnp_de" "dynn_Beauffremont" }
			{ "dynnp_de" "dynn_Polignac" }
			{ "dynnp_de" "dynn_Riencourt" }
			{ "dynnp_de" "dynn_Menthon" }
			{ "dynnp_d_" "dynn_Argouges" }
			{ "dynnp_de" "dynn_Noailles" }
			{ "dynnp_de" "dynn_Mostuejouls" }
			{ "dynnp_de" "dynn_Briey" }
			{ "dynnp_de" "dynn_VoguE_" }
			{ "dynnp_de" "dynn_Tournemire" }
			{ "dynnp_de" "dynn_CuriE_res" }
			{ "dynnp_de" "dynn_Beauvau" }
			"dynn_Chabot"
			{ "dynnp_de" "dynn_Menou" }
			{ "dynnp_de" "dynn_Saint-Gilles" }
			{ "dynnp_de" "dynn_Montrichard" }
			{ "dynnp_d_" "dynn_Abzac" }
			{ "dynnp_de" "dynn_Loubens" }
			{ "dynnp_de" "dynn_PontchA_teau" }
			{ "dynnp_de" "dynn_VitrE_" }
			{ "dynnp_d_" "dynn_Estouville" }
			{ "dynnp_de" "dynn_Mauvoisin" }
			{ "dynnp_de" "dynn_Blaison" }
			{ "dynnp_de" "dynn_Craon" }
			{ "dynnp_de" "dynn_Mayenne" }
			{ "dynnp_des" "dynn_Roches" }
			{ "dynnp_de" "dynn_SablE_" }
			{ "dynnp_d_" "dynn_Avesnes" }
			"dynn_ClE_ment"
			{ "dynnp_du" "dynn_Perche" }
			{ "dynnp_des" "dynn_Barres" }
			{ "dynnp_de" "dynn_Campistron" }
			{ "dynnp_de" "dynn_Marly" }
			{ "dynnp_de" "dynn_Melun" }
			{ "dynnp_de" "dynn_Montmorency" }
			{ "dynnp_de" "dynn_Nanteuil" }
			{ "dynnp_de" "dynn_Neauphle" }
			{ "dynnp_de" "dynn_Saint-Denis" }
			{ "dynnp_de" "dynn_Voisins" }
			{ "dynnp_de" "dynn_Boves" }
			{ "dynnp_de" "dynn_Fontaines" }
			{ "dynnp_de" "dynn_Roucy" }
			{ "dynnp_de" "dynn_Saint-ValE_ry" }
			{ "dynnp_de" "dynn_Bar" }
			{ "dynnp_de" "dynn_Villehardouin" }
			{ "dynnp_de" "dynn_Donzy" }
			{ "dynnp_d_" "dynn_Astarac" }
			{ "dynnp_de" "dynn_Montesquiou" }
			{ "dynnp_de" "dynn_Cardaillac" }
			{ "dynnp_de" "dynn_Gontaut" }
			{ "dynnp_de" "dynn_Roquefeuil" }
			{ "dynnp_de" "dynn_Barbaira" }
			{ "dynnp_de" "dynn_Durfort" }
			{ "dynnp_d_" "dynn_Hautpoul" }
			{ "dynnp_de" "dynn_Laurac" }
			{ "dynnp_de" "dynn_Saissac" }
			{ "dynnp_de" "dynn_Termes" }
			{ "dynnp_de" "dynn_Durban" }
			{ "dynnp_de" "dynn_Saone" }
			"dynn_Ledur"
			{ "dynnp_de" "dynn_Guise" }
			{ "dynnp_de" "dynn_Crysoing" }
			{ "dynnp_de" "dynn_Neuilly" }
			{ "dynnp_de" "dynn_Bazoches" }
			{ "dynnp_de" "dynn_Chappes" }
			{ "dynnp_de" "dynn_Toucy" }
			{ "dynnp_de" "dynn_Trith" }
			{ "dynnp_d_" "dynn_Apremont" }
			{ "dynnp_de" "dynn_Trainel" }
			{ "dynnp_de" "dynn_RhE_ninghe" }
			{ "dynnp_de" "dynn_Monteil" }
			{ "dynnp_de" "dynn_Bourbourg" }
			{ "dynnp_d_" "dynn_Ardres" }
			{ "dynnp_de" "dynn_Fiennes" }
			{ "dynnp_de" "dynn_Saint-Pol" }
			{ "dynnp_de" "dynn_Baudement" }
			{ "dynnp_d_" "dynn_Erville" }
			{ "dynnp_de" "dynn_Glane" }
			{ "dynnp_d_" "dynn_Apchier" }
			{ "dynnp_de" "dynn_Messines" }
			"dynn_La_FertE_"
			"dynn_Grenier"
			{ "dynnp_de" "dynn_Montfaucon" }
			{ "dynnp_de" "dynn_Plancy" }
			{ "dynnp_de" "dynn_Petra" }
			{ "dynnp_de" "dynn_Marash" }
			{ "dynnp_de" "dynn_Mousson" }
			{ "dynnp_d_" "dynn_Alamant" }
			{ "dynnp_de" "dynn_Cicon" }
			{ "dynnp_de" "dynn_Karditsa" }
			{ "dynnp_de" "dynn_Motluel" }
			{ "dynnp_de" "dynn_Salvaing" }
			{ "dynnp_de" "dynn_Thoire" }
			{ "dynnp_de" "dynn_Valromey" }
			{ "dynnp_de" "dynn_Lacarre" }
			{ "dynnp_de" "dynn_Fenouillet" }
			"dynn_PantalE_on"
			{ "dynnp_de" "dynn_Tarentaise" }
			{ "dynnp_d_" "dynn_Euse" }
			{ "dynnp_de" "dynn_Courtenay" }
			{ "dynnp_de" "dynn_Saint-Valery" }
			{ "dynnp_d_" "dynn_Enghien" }
			{ "dynnp_de" "dynn_Matha" }
			{ "dynnp_d_" "dynn_Arcy" }
			{ "dynnp_de" "dynn_BaugE_" }
			{ "dynnp_de" "dynn_Montboissier" }
			{ "dynnp_de" "dynn_Baugency" }
			{ "dynnp_de" "dynn_Tresmes" }
			{ "dynnp_de" "dynn_Courtenay" }
			{ "dynnp_d_" "dynn_Ibelin" }
			{ "dynnp_de" "dynn_Milly" }
			{ "dynnp_de" "dynn_Joinville" }
			{ "dynnp_de" "dynn_ChA_tillon" }
			{ "dynnp_de" "dynn_la_Roche" }
			{ "dynnp_d_" "dynn_Altaville" }
			{ "dynnp_de" "dynn_Malveisin" }
			{ "dynnp_de" "dynn_Soule" }
			{ "dynnp_de" "dynn_Leu" }
			{ "dynnp_de" "dynn_Coarraze" }
			{ "dynnp_de" "dynn_Lescar" }
			{ "dynnp_de" "dynn_Morlaas" }
			{ "dynnp_de" "dynn_Pau" }
			{ "dynnp_d_" "dynn_Orthez" }
			{ "dynnp_de" "dynn_Pardiac" }
			{ "dynnp_de" "dynn_Fezenzaguet" }
			{ "dynnp_de" "dynn_LuC_on" }
			{ "dynnp_de" "dynn_Talmont" }
			{ "dynnp_de" "dynn_GuE_ret" }
			{ "dynnp_de" "dynn_Martel" }
			{ "dynnp_de" "dynn_Cognac" }
			{ "dynnp_de" "dynn_Riom" }
			{ "dynnp_de" "dynn_Thiers" }
			{ "dynnp_de" "dynn_Guines" }
			{ "dynnp_de" "dynn_Artois" }
			{ "dynnp_de" "dynn_Evreux" }
			{ "dynnp_de" "dynn_Rennes" }
			{ "dynnp_de" "dynn_Penthievre" }
			{ "dynnp_de" "dynn_Vendome" }
			{ "dynnp_de" "dynn_Blois" }
			{ "dynnp_de" "dynn_Chartres" }
			{ "dynnp_d_" "dynn_I_le" }
			{ "dynnp_de" "dynn_Vermandois" }
			{ "dynnp_de" "dynn_Reims" }
			{ "dynnp_de" "dynn_Luxembourg" }
			{ "dynnp_d_" "dynn_Auxerre" }
			{ "dynnp_de" "dynn_Saintois" }
			{ "dynnp_de" "dynn_Besancon" }
			{ "dynnp_de" "dynn_Nevers" }
			{ "dynnp_de" "dynn_Tourraine" }
			{ "dynnp_de" "dynn_la_TrE_moille" }
			{ "dynnp_de" "dynn_Saintonge" }
			{ "dynnp_de" "dynn_Cilicia" }
			{ "dynnp_de" "dynn_Bourbon" }
			{ "dynnp_de" "dynn_Limousin" }
			{ "dynnp_d_" "dynn_Angouleme" }
			{ "dynnp_de" "dynn_Macon" }
			{ "dynnp_de" "dynn_Dauphine" }
			{ "dynnp_de" "dynn_Monferrato" }
			{ "dynnp_de" "dynn_Valais" }
			{ "dynnp_de" "dynn_Grisons" }
			{ "dynnp_de" "dynn_Chur" }
			{ "dynnp_de" "dynn_FE_zensaguet" }
			{ "dynnp_de" "dynn_Molle" }
			{ "dynnp_de" "dynn_Faucogney" }
			{ "dynnp_de" "dynn_Ridefort" }
			{ "dynnp_de" "dynn_Moulin" }
			{ "dynnp_de" "dynn_Chacim" }
			{ "dynnp_de" "dynn_Montreuil" }
			{ "dynnp_de" "dynn_Montoire" }
			{ "dynnp_de" "dynn_Clissa" }
			"dynn_Mercadier"
			{ "dynnp_de" "dynn_Vaqueiras" }
			"dynn_St__Genes"
			{ "dynnp_de" "dynn_Sancerre" }
			"dynn_Vienne"
			{ "dynnp_de" "dynn_Dammartin" }
			{ "dynnp_de" "dynn_Coligny" }
			{ "dynnp_de" "dynn_Lautrec" }
			{ "dynnp_d_" "dynn_Amboise" }
			{ "dynnp_de" "dynn_Saint-Pol" }
			"dynn_Saint-Menehould"
			{ "dynnp_de" "dynn_Bruyeres" }
			{ "dynnp_de" "dynn_Stromoncourt" }
			{ "dynnp_de" "dynn_Champlitte" }
			{ "dynnp_de" "dynn_Tries" }
			{ "dynnp_du" "dynn_Pay_de_Provence" }
			{ "dynnp_de" "dynn_Balben" }
			{ "dynnp_de" "dynn_Comps" }
			{ "dynnp_d_" "dynn_Aissailly" }
			{ "dynnp_de" "dynn_Murois" }
			{ "dynnp_of" "dynn_Syria" }
			"dynn_Borrel"
			{ "dynnp_de" "dynn_Aspa" }
			{ "dynnp_de" "dynn_Naplouse" }
			{ "dynnp_de" "dynn_Donjon" }
			{ "dynnp_le" "dynn_Rat" }
			{ "dynnp_de" "dynn_Montaigu" }
			{ "dynnp_de" "dynn_Thercy" }
			{ "dynnp_de" "dynn_Montacute" }
			{ "dynnp_de" "dynn_Ville-Bride" }
			{ "dynnp_de" "dynn_Chateauneuf" }
			{ "dynnp_de" "dynn_Revel" }
			{ "dynnp_de" "dynn_Lorgue" }
			{ "dynnp_de" "dynn_Villiers" }
			{ "dynnp_de" "dynn_Pins" }
			{ "dynnp_de" "dynn_Villaret" }
			{ "dynnp_de" "dynn_la_Marck" }
			{ "dynnp_de" "dynn_Thourotte" }
			{ "dynnp_de" "dynn_Roussillon" }
			{ "dynnp_de" "dynn_Pontigny" }
			{ "dynnp_de" "dynn_Beauvoir" }
			{ "dynnp_de" "dynn_Marcey" }
			{ "dynnp_d_" "dynn_Aix" }
			{ "dynnp_de" "dynn_ME_didan" }
			{ "dynnp_de" "dynn_Sarvay" }
			{ "dynnp_de" "dynn_Ruvigny" }
			{ "dynnp_de" "dynn_Richercourt" }
			{ "dynnp_de" "dynn_BlA_mont" }
			{ "dynnp_de" "dynn_Neuville" }
			{ "dynnp_de" "dynn_TraI_nel" }
			{ "dynnp_de" "dynn_Ponthieu" }
			{ "dynnp_de" "dynn_Sully" }
			{ "dynnp_of" "dynn_Burgundy" }
		}

		dynasty_names = {
			"dynn_Fournier"
			{ "dynnp_du" "dynn_Dros" }
			{ "dynnp_de" "dynn_Piemont" }
			{ "dynnp_de" "dynn_Bage" }
			{ "dynnp_de" "dynn_Montferrat" }
			{ "dynnp_de" "dynn_Luxembourg" }
			{ "dynnp_d_" "dynn_Anjou" }
			"dynn_Chamillart"
			{ "dynnp_de" "dynn_ChA_tillion" }
			{ "dynnp_d_" "dynn_Avaugour" }
			{ "dynnp_de" "dynn_Dreaux" }
			{ "dynnp_de" "dynn_Dreux" }
			{ "dynnp_de" "dynn_Clermont" }
			{ "dynnp_de" "dynn_Vitry" }
			{ "dynnp_de" "dynn_Dampierre" }
			{ "dynnp_de" "dynn_Sully" }
			{ "dynnp_de" "dynn_Bourg" }
			{ "dynnp_de" "dynn_Gironde" }
			{ "dynnp_d_" "dynn_Artois" }
			{ "dynnp_de" "dynn_Valois" }
			{ "dynnp_de" "dynn_Guines" }
			"dynn_Saint-Gilles"
			{ "dynnp_d_" "dynn_E_olienne" }
			{ "dynnp_de" "dynn_Bessa" }
			{ "dynnp_de" "dynn_Ramla" }
			{ "dynnp_d_" "dynn_Ibelin" }
			"dynn_Luneville"
			{ "dynnp_de" "dynn_Cleriuex" }
			{ "dynnp_de" "dynn_Valpergue" }
			{ "dynnp_de" "dynn_Cuiseaux" }
			{ "dynnp_de" "dynn_Joinville" }
			{ "dynnp_de" "dynn_Vaudemont" }
			{ "dynnp_d_" "dynn_Hauteville" }
			{ "dynnp_de" "dynn_Narbonne" }
			{ "dynnp_de" "dynn_Bourgogne-ComtE_" }
			{ "dynnp_de" "dynn_l_Aigle" }
			{ "dynnp_de" "dynn_Flotte" }
			{ "dynnp_de" "dynn_Vendome" }
			{ "dynnp_de" "dynn_Champagne" }
			{ "dynnp_de" "dynn_Namur" }
			{ "dynnp_de" "dynn_Bissy" }
			{ "dynnp_d_" "dynn_Eu" }
			{ "dynnp_du" "dynn_Perche" }
			{ "dynnp_de" "dynn_Boulogne" }
			"dynn_Puy_du_Fou"
			{ "dynnp_de" "dynn_Preuilly" }
			{ "dynnp_de" "dynn_Laval" }
			{ "dynnp_de" "dynn_Salins" }
			{ "dynnp_de" "dynn_ME_ziriac" }
			{ "dynnp_d_" "dynn_Oisy" }
			{ "dynnp_de" "dynn_Barthe" }
			{ "dynnp_de" "dynn_Fezensac" }
			{ "dynnp_de" "dynn_Boisrobert" }
			{ "dynnp_de" "dynn_Bourbon" }
			{ "dynnp_d_" "dynn_AngoulE_me" }
			"dynn_Le_Tellier"
			{ "dynnp_de" "dynn_Talleyrand" }
			{ "dynnp_de" "dynn_Batarnay" }
			{ "dynnp_de" "dynn_Beaumont-au-Maine" }
			{ "dynnp_de" "dynn_Hainaut" }
			{ "dynnp_de" "dynn_Crecy" }
			{ "dynnp_de" "dynn_Bellegarde" }
			{ "dynnp_de" "dynn_St__Hilary" }
			{ "dynnp_d_" "dynn_Aubigny" }
			{ "dynnp_de" "dynn_Albemarle" }
			{ "dynnp_de" "dynn_Verre" }
			{ "dynnp_d_" "dynn_Evreux" }
			{ "dynnp_de" "dynn_Mellent" }
			{ "dynnp_de" "dynn_Baalun" }
			{ "dynnp_de" "dynn_Vernon" }
			"dynn_Estouteville"
			"dynn_Nicolay"
			{ "dynnp_of" "dynn_Burgundy" }
			{ "dynnp_de" "dynn_Vexin-Amiens" }
			{ "dynnp_de" "dynn_Nimes" }
			{ "dynnp_de" "dynn_Tilly" }
			{ "dynnp_de" "dynn_Mons" }
			{ "dynnp_de" "dynn_Hainault" }
			{ "dynnp_de" "dynn_Montaigu" }
			{ "dynnp_de" "dynn_Benserade" }
			"dynn_Barrois"
			{ "dynnp_de" "dynn_Harcourt" }
			"dynn_Matfrieding"
			{ "dynnp_de" "dynn_Carolui" }
			{ "dynnp_de" "dynn_Lagery" }
			"dynn_Litpolden"
			{ "dynnp_von" "dynn_Franken" }
			{ "dynnp_de" "dynn_Maurienne" }
			{ "dynnp_de" "dynn_Vienne" }
			{ "dynnp_de" "dynn_Foix" }
			{ "dynnp_de" "dynn_Lorraine" }
			{ "dynnp_de" "dynn_Vermandois" }
			{ "dynnp_de" "dynn_Blois" }
			{ "dynnp_de" "dynn_Gastinois" }
			{ "dynnp_de" "dynn_Guincamp" }
			{ "dynnp_de" "dynn_Bretagne" }
			{ "dynnp_de" "dynn_Boullion" }
			{ "dynnp_de" "dynn_Longwy" }
			{ "dynnp_de" "dynn_Macon" }
			{ "dynnp_de" "dynn_Roucy" }
			{ "dynnp_de" "dynn_Heismes" }
			{ "dynnp_de" "dynn_SE_mur-en-Brionnais" }
			"dynn_Fergant"
			{ "dynnp_de" "dynn_Bolougne" }
			{ "dynnp_d_" "dynn_Antoing" }
			{ "dynnp_d_" "dynn_Espagne" }
			{ "dynnp_d_" "dynn_Arc" }
			"dynn_Gladius_Christi"
			{ "dynnp_de" "dynn_Mortemart" }
			{ "dynnp_de" "dynn_la_Rochefoucauld" }
			{ "dynnp_de" "dynn_Caumont" }
			{ "dynnp_de" "dynn_Serrant" }
			{ "dynnp_de" "dynn_la_Roche_Aymon" }
			{ "dynnp_de" "dynn_LE_vis" }
			{ "dynnp_de" "dynn_Villeneuve" }
			{ "dynnp_de" "dynn_Beauffremont" }
			{ "dynnp_de" "dynn_Polignac" }
			{ "dynnp_de" "dynn_Riencourt" }
			{ "dynnp_de" "dynn_Menthon" }
			{ "dynnp_d_" "dynn_Argouges" }
			{ "dynnp_de" "dynn_Noailles" }
			{ "dynnp_de" "dynn_Mostuejouls" }
			{ "dynnp_de" "dynn_Briey" }
			{ "dynnp_de" "dynn_VoguE_" }
			{ "dynnp_de" "dynn_Tournemire" }
			{ "dynnp_de" "dynn_CuriE_res" }
			{ "dynnp_de" "dynn_Beauvau" }
			"dynn_Chabot"
			{ "dynnp_de" "dynn_Menou" }
			{ "dynnp_de" "dynn_Saint-Gilles" }
			{ "dynnp_de" "dynn_Montrichard" }
			{ "dynnp_d_" "dynn_Abzac" }
			{ "dynnp_de" "dynn_Loubens" }
			{ "dynnp_de" "dynn_PontchA_teau" }
			{ "dynnp_de" "dynn_VitrE_" }
			{ "dynnp_d_" "dynn_Estouville" }
			{ "dynnp_de" "dynn_Mauvoisin" }
			{ "dynnp_de" "dynn_Blaison" }
			{ "dynnp_de" "dynn_Craon" }
			{ "dynnp_de" "dynn_Mayenne" }
			{ "dynnp_des" "dynn_Roches" }
			{ "dynnp_de" "dynn_SablE_" }
			{ "dynnp_d_" "dynn_Avesnes" }
			"dynn_ClE_ment"
			{ "dynnp_du" "dynn_Perche" }
			{ "dynnp_des" "dynn_Barres" }
			{ "dynnp_de" "dynn_Campistron" }
			{ "dynnp_de" "dynn_Marly" }
			{ "dynnp_de" "dynn_Melun" }
			{ "dynnp_de" "dynn_Montmorency" }
			{ "dynnp_de" "dynn_Nanteuil" }
			{ "dynnp_de" "dynn_Neauphle" }
			{ "dynnp_de" "dynn_Saint-Denis" }
			{ "dynnp_de" "dynn_Voisins" }
			{ "dynnp_de" "dynn_Boves" }
			{ "dynnp_de" "dynn_Fontaines" }
			{ "dynnp_de" "dynn_Roucy" }
			{ "dynnp_de" "dynn_Saint-ValE_ry" }
			{ "dynnp_de" "dynn_Bar" }
			{ "dynnp_de" "dynn_Villehardouin" }
			{ "dynnp_de" "dynn_Donzy" }
			{ "dynnp_d_" "dynn_Astarac" }
			{ "dynnp_de" "dynn_Montesquiou" }
			{ "dynnp_de" "dynn_Cardaillac" }
			{ "dynnp_de" "dynn_Gontaut" }
			{ "dynnp_de" "dynn_Roquefeuil" }
			{ "dynnp_de" "dynn_Barbaira" }
			{ "dynnp_de" "dynn_Durfort" }
			{ "dynnp_d_" "dynn_Hautpoul" }
			{ "dynnp_de" "dynn_Laurac" }
			{ "dynnp_de" "dynn_Saissac" }
			{ "dynnp_de" "dynn_Termes" }
			{ "dynnp_de" "dynn_Durban" }
			{ "dynnp_de" "dynn_Saone" }
			"dynn_Ledur"
			{ "dynnp_de" "dynn_Guise" }
			{ "dynnp_de" "dynn_Crysoing" }
			{ "dynnp_de" "dynn_Neuilly" }
			{ "dynnp_de" "dynn_Bazoches" }
			{ "dynnp_de" "dynn_Chappes" }
			{ "dynnp_de" "dynn_Toucy" }
			{ "dynnp_de" "dynn_Trith" }
			{ "dynnp_d_" "dynn_Apremont" }
			{ "dynnp_de" "dynn_Trainel" }
			{ "dynnp_de" "dynn_RhE_ninghe" }
			{ "dynnp_de" "dynn_Monteil" }
			{ "dynnp_de" "dynn_Bourbourg" }
			{ "dynnp_d_" "dynn_Ardres" }
			{ "dynnp_de" "dynn_Fiennes" }
			{ "dynnp_de" "dynn_Saint-Pol" }
			{ "dynnp_de" "dynn_Baudement" }
			{ "dynnp_d_" "dynn_Erville" }
			{ "dynnp_de" "dynn_Glane" }
			{ "dynnp_d_" "dynn_Apchier" }
			{ "dynnp_de" "dynn_Messines" }
			"dynn_La_FertE_"
			"dynn_Grenier"
			{ "dynnp_de" "dynn_Montfaucon" }
			{ "dynnp_de" "dynn_Plancy" }
			{ "dynnp_de" "dynn_Petra" }
			{ "dynnp_de" "dynn_Marash" }
			{ "dynnp_de" "dynn_Mousson" }
			{ "dynnp_d_" "dynn_Alamant" }
			{ "dynnp_de" "dynn_Cicon" }
			{ "dynnp_de" "dynn_Karditsa" }
			{ "dynnp_de" "dynn_Motluel" }
			{ "dynnp_de" "dynn_Salvaing" }
			{ "dynnp_de" "dynn_Thoire" }
			{ "dynnp_de" "dynn_Valromey" }
			{ "dynnp_de" "dynn_Lacarre" }
			{ "dynnp_de" "dynn_Fenouillet" }
			"dynn_PantalE_on"
			{ "dynnp_de" "dynn_Tarentaise" }
			{ "dynnp_d_" "dynn_Euse" }
			{ "dynnp_de" "dynn_Courtenay" }
			{ "dynnp_de" "dynn_Saint-Valery" }
			{ "dynnp_d_" "dynn_Enghien" }
			{ "dynnp_de" "dynn_Matha" }
			{ "dynnp_d_" "dynn_Arcy" }
			{ "dynnp_de" "dynn_BaugE_" }
			{ "dynnp_de" "dynn_Montboissier" }
			{ "dynnp_de" "dynn_Baugency" }
			{ "dynnp_de" "dynn_Tresmes" }
			{ "dynnp_de" "dynn_Courtenay" }
			{ "dynnp_d_" "dynn_Ibelin" }
			{ "dynnp_de" "dynn_Milly" }
			{ "dynnp_de" "dynn_Joinville" }
			{ "dynnp_de" "dynn_ChA_tillon" }
			{ "dynnp_de" "dynn_la_Roche" }
			{ "dynnp_d_" "dynn_Altaville" }
			{ "dynnp_de" "dynn_Malveisin" }
			{ "dynnp_de" "dynn_Soule" }
			{ "dynnp_de" "dynn_Leu" }
			{ "dynnp_de" "dynn_Coarraze" }
			{ "dynnp_de" "dynn_Lescar" }
			{ "dynnp_de" "dynn_Morlaas" }
			{ "dynnp_de" "dynn_Pau" }
			{ "dynnp_d_" "dynn_Orthez" }
			{ "dynnp_de" "dynn_Pardiac" }
			{ "dynnp_de" "dynn_Fezenzaguet" }
			{ "dynnp_de" "dynn_LuC_on" }
			{ "dynnp_de" "dynn_Talmont" }
			{ "dynnp_de" "dynn_GuE_ret" }
			{ "dynnp_de" "dynn_Martel" }
			{ "dynnp_de" "dynn_Cognac" }
			{ "dynnp_de" "dynn_Riom" }
			{ "dynnp_de" "dynn_Thiers" }
			{ "dynnp_de" "dynn_Guines" }
			{ "dynnp_de" "dynn_Artois" }
			{ "dynnp_de" "dynn_Evreux" }
			{ "dynnp_de" "dynn_Rennes" }
			{ "dynnp_de" "dynn_Penthievre" }
			{ "dynnp_de" "dynn_Vendome" }
			{ "dynnp_de" "dynn_Blois" }
			{ "dynnp_de" "dynn_Chartres" }
			{ "dynnp_d_" "dynn_I_le" }
			{ "dynnp_de" "dynn_Vermandois" }
			{ "dynnp_de" "dynn_Reims" }
			{ "dynnp_de" "dynn_Luxembourg" }
			{ "dynnp_d_" "dynn_Auxerre" }
			{ "dynnp_de" "dynn_Saintois" }
			{ "dynnp_de" "dynn_Besancon" }
			{ "dynnp_de" "dynn_Nevers" }
			{ "dynnp_de" "dynn_Tourraine" }
			{ "dynnp_de" "dynn_la_TrE_moille" }
			{ "dynnp_de" "dynn_Saintonge" }
			{ "dynnp_de" "dynn_Cilicia" }
			{ "dynnp_de" "dynn_Bourbon" }
			{ "dynnp_de" "dynn_Limousin" }
			{ "dynnp_d_" "dynn_Angouleme" }
			{ "dynnp_de" "dynn_Macon" }
			{ "dynnp_de" "dynn_Dauphine" }
			{ "dynnp_de" "dynn_Monferrato" }
			{ "dynnp_de" "dynn_Valais" }
			{ "dynnp_de" "dynn_Grisons" }
			{ "dynnp_de" "dynn_Chur" }
			{ "dynnp_de" "dynn_FE_zensaguet" }
			{ "dynnp_de" "dynn_Molle" }
			{ "dynnp_de" "dynn_Faucogney" }
			{ "dynnp_de" "dynn_Ridefort" }
			{ "dynnp_de" "dynn_Moulin" }
			{ "dynnp_de" "dynn_Chacim" }
			{ "dynnp_de" "dynn_Montreuil" }
			{ "dynnp_de" "dynn_Montoire" }
			{ "dynnp_de" "dynn_Clissa" }
			"dynn_Mercadier"
			{ "dynnp_de" "dynn_Vaqueiras" }
			"dynn_St__Genes"
			{ "dynnp_de" "dynn_Sancerre" }
			"dynn_Vienne"
			{ "dynnp_de" "dynn_Dammartin" }
			{ "dynnp_de" "dynn_Coligny" }
			{ "dynnp_de" "dynn_Lautrec" }
			{ "dynnp_d_" "dynn_Amboise" }
			{ "dynnp_de" "dynn_Saint-Pol" }
			"dynn_Saint-Menehould"
			{ "dynnp_de" "dynn_Bruyeres" }
			{ "dynnp_de" "dynn_Stromoncourt" }
			{ "dynnp_de" "dynn_Champlitte" }
			{ "dynnp_de" "dynn_Tries" }
			{ "dynnp_du" "dynn_Pay_de_Provence" }
			{ "dynnp_de" "dynn_Balben" }
			{ "dynnp_de" "dynn_Comps" }
			{ "dynnp_d_" "dynn_Aissailly" }
			{ "dynnp_de" "dynn_Murois" }
			{ "dynnp_of" "dynn_Syria" }
			"dynn_Borrel"
			{ "dynnp_de" "dynn_Aspa" }
			{ "dynnp_de" "dynn_Naplouse" }
			{ "dynnp_de" "dynn_Donjon" }
			{ "dynnp_le" "dynn_Rat" }
			{ "dynnp_de" "dynn_Montaigu" }
			{ "dynnp_de" "dynn_Thercy" }
			{ "dynnp_de" "dynn_Montacute" }
			{ "dynnp_de" "dynn_Ville-Bride" }
			{ "dynnp_de" "dynn_Chateauneuf" }
			{ "dynnp_de" "dynn_Revel" }
			{ "dynnp_de" "dynn_Lorgue" }
			{ "dynnp_de" "dynn_Villiers" }
			{ "dynnp_de" "dynn_Pins" }
			{ "dynnp_de" "dynn_Villaret" }
			{ "dynnp_de" "dynn_la_Marck" }
			{ "dynnp_de" "dynn_Thourotte" }
			{ "dynnp_de" "dynn_Roussillon" }
			{ "dynnp_de" "dynn_Pontigny" }
			{ "dynnp_de" "dynn_Beauvoir" }
			{ "dynnp_de" "dynn_Marcey" }
			{ "dynnp_d_" "dynn_Aix" }
			{ "dynnp_de" "dynn_ME_didan" }
			{ "dynnp_de" "dynn_Sarvay" }
			{ "dynnp_de" "dynn_Ruvigny" }
			{ "dynnp_de" "dynn_Richercourt" }
			{ "dynnp_de" "dynn_BlA_mont" }
			{ "dynnp_de" "dynn_Neuville" }
			{ "dynnp_de" "dynn_TraI_nel" }
			{ "dynnp_de" "dynn_Ponthieu" }
			{ "dynnp_de" "dynn_Sully" }
			{ "dynnp_of" "dynn_Burgundy" }
		}

		male_names = {
			E_douard E_rrard E_tienne Adalbert AdE_mar Adrien Aimery Alain Aldebert AldE_ric Alphonse Amaury
			AmE_dE_e Ancel AndrE_ Angelbert Antoine Archambaud Arnaud Arnault Arnoul Aubry Aymar BarthE_lE_mi
			Baudouin BenoI_t BE_renger Bernard Bertrand Bohemond Boson Bouchard Centule Charles
			Clotaire Ebbon Enguerrand Eudes Eustache Evrard Foulques FranC_ois FrE_dE_ric GE_raud Gargamel Gaucher Gaucelin
			Gauthier Geoffroy GE_raud Gelduin Gilbert Gilles Godefroy Guichard Guiges Guilhem Guillaume
			Guy HE_lie Hamelin Henri Herbert Hildebert Hugues Humbert Jacques JaufrE_ Jaspert Jean Josselin
			Jourdain Julien LE_on LE_onard Lothaire Louis Loup ManassE_s Mathieu Maurice Nicolas Ogier Onfroy Orson Othon
			Payen Philippe Pierre Raimbaut Raoul Raymond Raynaud Renaud Richard Robert Robin Roger
			Rorgon Rorgues Roubaud Savary Sigismond Simon Thibault ThiE_baut Thierry Thomas Valeran Yves
		}
		female_names = {
			E_lE_onore E_lodie E_tiennette AdE_le Adalmode Adelaide Adelinde Agathe AgnE_s AlE_arde Alice AliE_nor
			Alix Almodis Amelie Anne Antoinette Arsinde Aude Aurengarde BE_atrice BE_atrix Belleassez BenoI_te BE_rengE_re
			Berthe Blanche Bonne Bourgogne Bourguigne CE_cile CathE_rine Charlotte Constance Denise Douce Echive Eglantine
			Elisabeth Emma Ermengarde Ermessinde Esclarmonde Euphrosine Eustachie Eve Gerberge
			GisE_le Guillaumette HE_loise Helvis Hodierne Ide Ida IldE_garde Isabeau Isabelle Jeanne Judith
			Julienne Mafalda Mahaut Margot Marguerite Marie Marthe Mascarose Mathilde ME_lisande
			ME_lisende ME_lusine PE_ronelle Pernelle Perinne Pernette Plaisance Raymonde Sarrazine SolE_ne Sophie
			StE_phanie Sybille Tiburge Valence Yolande
		}
		dynasty_of_location_prefix = "dynnp_de"
		grammar_transform = french
		#patronym_prefix_male = "dynnpat_pre_fils"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 60
		mat_grf_name_chance = 10
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 60
		mother_name_chance = 10
		
		ethnicities = {
			10 = caucasian_blond
			5 = caucasian_ginger
			45 = caucasian_brown_hair
			35 = caucasian_dark_hair
		}
		
		mercenary_names = {
			{ name = "mercenary_company_tard_venus_1" }
			{ name = "mercenary_company_french_band_1" }
			{ name = "mercenary_company_routiers_1" }
			{ name = "mercenary_company_french_band_2" }
		}
	}
}