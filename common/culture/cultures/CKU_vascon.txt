vascon_group = {
	graphical_cultures = {
		iberian_group_coa_gfx
		mediterranean_building_gfx
		western_clothing_gfx
		western_unit_gfx
	}

	basque = {
		color = rgb { 41 146 17 } 
		graphical_cultures = {
			iberian_group_coa_gfx
			mediterranean_building_gfx
			western_clothing_gfx
			western_unit_gfx
		}
		ethnicities = {
			10 = mediterranean
		}

		mercenary_names = {
			{ name = "mercenary_company_nafarroako_konpainia" coat_of_arms = "mc_nafarroako_konpainia" }
		}
		
		cadet_dynasty_names = {
		}

		dynasty_names = {
		}

		male_names = {
			Alvar Antso Aznar Alexander Alfontso Alontso Ander Antselmo Andoni Arturo BeltrA_n Bermudo Eneko Armengol Kalixto Karlos Igandea DurA_n Galindo 
			Kasio Henrike Exteban Fadrike Felix Felipe Ferran Fruela Julen Aitor Gartzia Gaspar Gome Untzal Gilen Hektor Ignazio Jagoba Jaime Xabier Ximeno 
			Jokim Gorka Joseba Jon Latzaro Laurendi Lukas Manrike Imanol Martzial Markos MartI_n Mateo Mendo Munio Nikanor NuN_o OrdoN_o Obeko Paulo Paskual 
			Peru Pelaio Pontze Errapel Ramiro Erramun Rikardo Edriko Gaizka Jakue Santxo Suero Tello Telmo Bela Biktor Bixente Ager Haimar Anaut Asier Mikel 
			Markel Koldo Ibai Igor Aritz BeN_at Edorta Eloi Ipar Unai Beltso Eder Izan Oier Frantzisko Urko
		}
		female_names = {
		Urtsula Urraka Beronika Josune Santxa ON_a Oria Aldontza Anderkina Antsa Ava Beatritz Belaska Berenguela Briska Elbire Eliza Endultzia Jone KariN_e 
		Konstantzia Leonor Maior Miren Mentzia Munia NuN_a Oneka Oria Plazentzia Toda Urraka Ximena Alitzia Monika Frantziska Gadea Gotina Guiomar Ane DueN_a 
		Karmena Amale Itzal Goizane Ainara Ainhoa Arantxa Amaia Edurne Olaia Gabone Idoia Irati Itziar Leire Maialen Maite Milia Nagore Nora Oihana SabiN_e 
		Saioa Sarabia Taresa Zuria Uxue GarbiN_e BegoN_a
		}
		
		dynasty_of_location_prefix = "dynnp_de"
		patronym_suffix_male = "dynnpat_suf_iz"
		patronym_suffix_female = "dynnpat_suf_iz"
		always_use_patronym = yes	

		pat_grf_name_chance = 35
		mat_grf_name_chance = 15
		father_name_chance = 15
		pat_grm_name_chance = 35
		mat_grm_name_chance = 15
		mother_name_chance = 15
	}