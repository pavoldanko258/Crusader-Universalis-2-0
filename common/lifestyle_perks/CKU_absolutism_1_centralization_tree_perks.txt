# CKU ABSOLUTISM PERKS
# 1. NO REQUIREMENTS
# 2. FOCUS 1
# 3. FOCUS 2
# 4. FOCUS 3

#No requirements
CKU_consolidate_royal_holdings_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 7 0 }
	icon = node_absolutism
	
	character_modifier = {
		domain_limit = 2
	}
	
	auto_selection_weight = {
		value = 0
	}
}
CKU_legit_rule_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 1 1 }
	icon = node_absolutism
	
	character_modifier = {
		short_reign_duration_mult = -0.15
		monthly_prestige_gain_mult = 0.05
	}
	
	parent = CKU_consolidate_royal_holdings_perk
}
CKU_safeguard_succession_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 1 3 }
	icon = node_absolutism
	
	character_modifier = {
		long_reign_duration_mult = 0.15
		monthly_dynasty_prestige_mult = 0.05
	}
	
	parent = CKU_legit_rule_perk
}
CKU_title_marriage_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 0 4 }
	icon = node_absolutism
	
	character_modifier = {
		title_creation_cost_mult = -0.15
	}
	
	parent = CKU_safeguard_succession_perk
}
CKU_blue_blood_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 2 4 }
	icon = node_absolutism
	
	character_modifier = {
		attraction_opinion = 10
		positive_random_genetic_chance = 0.15
	}
	
	parent = CKU_safeguard_succession_perk
}
CKU_pure_bloodline_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 1 5 }
	icon = node_absolutism
	
	character_modifier = {
		genetic_trait_strengthen_chance = 0.05
		life_expectancy = 3
	}
	
	parent = CKU_title_marriage_perk
	parent = CKU_blue_blood_perk
}
CKU_basic_conscription_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 5 1 }
	icon = node_absolutism
	
	character_modifier = {
		vassal_levy_contribution_mult = 0.10
		feudal_government_vassal_opinion = -10
	}
	
	parent = CKU_consolidate_royal_holdings_perk
}
CKU_state_religion_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 9 1 }
	icon = node_absolutism
	
	character_modifier = {
		theocracy_government_vassal_opinion = 10
		same_faith_opinion = 10
		opinion_of_same_faith = 10
	}
	
	parent = CKU_consolidate_royal_holdings_perk
}
CKU_basic_centralization_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 13 1 }
	icon = node_absolutism
	
	character_modifier = {
		domain_limit = 3
		vassal_opinion = -5
	}
	
	parent = CKU_consolidate_royal_holdings_perk
}

# TREE 2: FIRST FOCUS; MILITARY
CKU_organised_armies_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 5 2 }
	icon = node_absolutism
	
	character_modifier = {
		army_maintenance_mult = -0.5
		feudal_government_vassal_opinion = -10
	}
	
	parent = CKU_basic_conscription_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_1
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_1 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_organised_armies_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_extensive_conscription_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 3.5 3 }
	icon = node_absolutism
	
	character_modifier = {
		army_maintenance_mult = -0.5
		vassal_opinion = -10
	}
	
	parent = CKU_organised_armies_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_1
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_1 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_extensive_conscription_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_increased_arms_efficiency_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 3.5 4 }
	icon = node_absolutism
	
	character_modifier = {
		random_advantage = 7
		skirmishers_damage_mult = 0.05
		heavy_infantry_damage_mult = 0.05
		pikemen_damage_mult = 0.5
	}
	
	parent = CKU_extensive_conscription_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_1
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_1 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_increased_arms_efficiency_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_war_hawks_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 5 3 }
	icon = node_absolutism
	
	character_modifier = {
		monthly_war_income_mult = 0.10
	}
	
	parent = CKU_organised_armies_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_1
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_1 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_war_hawks_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_royal_guard_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 5 4 }
	icon = node_absolutism
	
	character_modifier = {
		knight_limit = 5
		knight_effectiveness_mult = 0.15
	}
	
	parent = CKU_war_hawks_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_1
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_1 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_royal_guard_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_war_tactics_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 6.5 3 }
	icon = node_absolutism
	
	character_modifier = {
		advantage = 5
		hostile_county_attrition = -0.05
	}
	
	parent = CKU_organised_armies_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_1
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_1 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_war_tactics_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_flying_batteries_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 6.5 4 }
	icon = node_absolutism
	
	character_modifier = {
		movement_speed = 0.05
		enemy_hard_casualty_modfier = 0.05
	}
	
	parent = CKU_war_tactics_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_1
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_1 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_flying_batteries_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_levee_en_masse_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 5 5 }
	icon = node_absolutism
	
	character_modifier = {
		vassal_levy_contribution_mult = 0.10
		man_at_arms_limit = 3
		man_at_arms_cap = 3
		vassal_opinion = -15
	}
	
	parent = CKU_increased_arms_efficiency_perk
	parent = CKU_royal_guard_perk
	parent = CKU_flying_batteries_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_1
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_1 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_levee_en_masse_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}

# TREE 3: SECOND FOCUS; RELIGION
CKU_expel_other_religions_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 9 2 }
	icon = node_absolutism
	
	character_modifier = {
		opinion_of_different_faith = -10
		different_faith_opinion = -10
		monthly_piety_gain_mult = 0.15
	}
	
	parent = CKU_state_religion_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_2
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_2 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_expel_other_religions_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_censorship_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 8 3 }
	icon = node_absolutism
	
	character_modifier = {
		cultural_head_fascination_mult = 0.05
		same_faith_opinion = 10
		opinion_of_same_faith = 10
	}
	
	parent = CKU_expel_other_religions_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_2
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_2 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_censorship_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_support_arts_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 8 4 }
	icon = node_absolutism
	
	character_modifier = {
		monthly_prestige = 1
	}
	
	parent = CKU_censorship_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_2
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_2 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_support_arts_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_one_country_one_faith_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 10 3 }
	icon = node_absolutism
	
	character_modifier = {
		different_faith_opinion = -10
		opinion_of_different_faith = -10
		domain_tax_mult = 0.05
	}
	
	parent = CKU_expel_other_religions_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_2
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_2 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_one_country_one_faith_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_religious_privileges_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 10 4 }
	icon = node_absolutism
	
	character_modifier = {
		levy_reinforcement_rate_same_faith = 0.10
		feudal_government_vassal_opinion = -10
	}
	
	parent = CKU_one_country_one_faith_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_2
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_2 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_religious_privileges_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_center_of_universe_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 9 5 }
	icon = node_absolutism
	
	character_modifier = {
		diplomacy_per_piety_level = 3
		theocracy_government_vassal_opinion = -20
	}
	
	parent = CKU_support_arts_perk
	parent = CKU_religious_privileges_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_2
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_2 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_center_of_universe_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_patronage_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 8 6 }
	icon = node_absolutism
	
	character_modifier = {
		monthly_dynasty_prestige_mult = 0.10
		monthly_prestige = 2.5
		attraction_opinion = 5
	}
	
	parent = CKU_center_of_universe_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_2
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_2 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_patronage_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_palaces_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 10 6 }
	icon = node_absolutism
	
	character_modifier = {
		piety_level_impact_mult = 0.15
		monthly_prestige_from_buildings_mult = 0.20
	}
	
	parent = CKU_center_of_universe_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_2
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_2 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_palaces_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
# TREE 3: THIRD FOCUS: ECONOMY AND DOMAIN
CKU_efficient_taxes_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 13 2 }
	icon = node_absolutism
	
	character_modifier = {
		domain_tax_mult = 0.15
	}
	
	parent = CKU_basic_centralization_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_3
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_3 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_efficient_taxes_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_improved_centralization_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 12 3 }
	icon = node_absolutism
	
	character_modifier = {
		domain_limit = 3
		vassal_opinion = -10
	}
	
	parent = CKU_efficient_taxes_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_3
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_3 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_improved_centralization_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_extensive_bureaucracy_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 12 4 }
	icon = node_absolutism
	
	character_modifier = {
		domain_tax_mult = 0.10
		build_speed = 0.20
	}
	
	parent = CKU_improved_centralization_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_3
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_3 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_extensive_bureaucracy_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_centralized_authority_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 12 5 }
	icon = node_absolutism
	
	character_modifier = {
		domain_limit = 3
		vassal_opinion = -15
	}
	
	parent = CKU_extensive_bureaucracy_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_3
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_3 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_centralized_authority_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_capital_growth_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 14 3 }
	icon = node_absolutism
	
	character_modifier = {
		character_capital_county_monthly_development_growth_add = 0.3
	}
	
	parent = CKU_efficient_taxes_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_3
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_3 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_capital_growth_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_direct_taxes_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 14 4 }
	icon = node_absolutism
	
	character_modifier = {
		vassal_tax_mult = 0.25
		vassal_opinion = -20
	}
	
	parent = CKU_capital_growth_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_3
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_3 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_direct_taxes_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_increased_control_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 14 5 }
	icon = node_absolutism
	
	character_modifier = {
		monthly_county_control_change_add = 0.5
		men_at_arms_maintenance_per_dread_mult = -0.05
	}
	
	parent = CKU_direct_taxes_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_3
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_3 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_increased_control_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}
CKU_extensive_domains_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 13 6 }
	icon = node_absolutism
	
	character_modifier = {
		domain_limit = 6
		domain_tax_mult = 0.10
		vassal_opinion = -15
	}
	
	parent = CKU_centralized_authority_perk
	parent = CKU_increased_control_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_3
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_3 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_extensive_domains_perk
			add_absolutism_lifestyle_perk_points = 1
		}
	}
}

#TRAIT PERKS
CKU_dynastical_marriage_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 1 7 }
	icon = trait_absolutist
	
	parent = CKU_pure_bloodline_perk
	
	effect = {
		add_trait_force_tooltip = absolutist
	}
}
CKU_grande_armee_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 5 7 }
	icon = trait_grande_armee
	
	parent = CKU_levee_en_masse_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_1
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_1 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_apres_moi_le_deluge_perk
			add_absolutism_lifestyle_perk_points = 1
		}
		else_if = {
			limit = {
				has_focus = absolutism_focus_1
			}
			add_trait_force_tooltip = grande_armee
		}
	}
}
CKU_apres_moi_le_deluge_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 9 7 }
	icon = trait_deluge
	
	parent = CKU_patronage_perk
	parent = CKU_palaces_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_2
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_2 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_apres_moi_le_deluge_perk
			add_absolutism_lifestyle_perk_points = 1
		}
		else_if = {
			limit = {
				has_focus = absolutism_focus_2
			}
			add_trait_force_tooltip = apres_moi_le_deluge
		}
	}
}
CKU_mercantilism_perk = {
	lifestyle = absolutism_lifestyle
	tree = centralization
	position = { 13 7 }
	icon = trait_mercantilist
	
	parent = CKU_extensive_domains_perk
	
	auto_selection_weight = {
		value = 0
		if = {
			limit = {
				has_focus = absolutism_focus_3
			}
			add = 10
		}
	}
	effect = {
		if = {
			limit = {
				NOT = { has_focus = absolutism_focus_3 }
			}
			custom_description_no_bullet = {
				text = requirements_not_met_text
			}
			remove_perk = CKU_mercantilism_perk
			add_absolutism_lifestyle_perk_points = 1
		}
		else_if = {
			limit = {
				has_focus = absolutism_focus_3
			}
			add_trait_force_tooltip = mercantilist
		}
	}
}







