on_death = {
	on_actions = {
		absolutism_inheritance
	}
}
absolutism_inheritance = {
	effect = {
		primary_heir = {
			add_absolutism_lifestyle_perk_points = { value = prev.absolutism_lifestyle_perks }
		}
	}
}

yearly_playable_pulse = {
	on_actions = {
		absolutism_experience
	}
}
absolutism_experience = {
	effect = {
		if = {
			limit = {
				has_lifestyle = absolutism_lifestyle
				is_independent_ruler = yes
			}
			add_absolutism_lifestyle_xp = 300
		}
		if = {
			limit = {
				is_independent_ruler = yes
				NOT = {
					has_lifestyle = absolutism_lifestyle
				}
			}
			add_absolutism_lifestyle_xp = 120
		}
	}
}