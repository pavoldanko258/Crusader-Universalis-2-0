﻿county_gov = 1
duchy_gov = 2
kingdom_gov = 5
empire_gov = 10
office_slots = 0

governor_value = {
	value = 0

	if = {
		limit = {
			highest_held_title_tier = tier_county
		}
		add = county_gov 
	}
	if = {
		limit = {
			highest_held_title_tier = tier_duchy
		}
		add = duchy_gov
	}
	if = {
		limit = {
			highest_held_title_tier = tier_kingdom
		}
		add = kingdom_gov
	}
	if = {
		limit = {
			highest_held_title_tier = tier_empire
		}
		add = empire_gov
	}			
}
governor_land_value = {
	value = 0

	if = {
		limit = {
			tier = tier_county
		}
		add = county_gov 
	}
	if = {
		limit = {
			tier = tier_duchy
		}
		add = duchy_gov
	}
			
}
#council_scaled_monthly_income = {
#	add = liege.highest_held_title_tier
#	subtract = 2
#	min = 0.5
#}
gov_salary = {
  value = 0
	if = {
		limit = {
			gov_salary2 < 41
		}
		add = 1
	}
	if = {
		limit = {
			gov_salary2 > 40
		}
		add = gov_salary3
	}	
}

gov_salary2 = {
  value = 0
	every_in_list = {
		variable = govern_titles
		add = development_level
	}  
}
gov_salary3 = {
  value = 0
	every_in_list = {
		variable = govern_titles
		add = development_level
		divide = 40
	}  
}