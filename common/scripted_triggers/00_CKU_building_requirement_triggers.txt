﻿######################
# GUNPOWDER MILITARY #
######################

building_gunnery_range_requirement_terrain = {
	OR = {
		terrain = savanna
		terrain = semi_desert
		terrain = tropical_rainforest
		terrain = tropical_floodplain
		terrain = forest
		terrain = grassland
		terrain = shrubland
		terrain = taiga
		terrain = rainforest_hills
		terrain = semi_desert_hills
		terrain = savanna_hills
		terrain = forest_hills
		terrain = grassland_hills
		terrain = shrubland_hills
		terrain = taiga_hills
		terrain = montane_forest
		terrain = montane_grassland
		terrain = montane_taiga
		terrain = farmlands
		terrain = wetlands
	}
}
