﻿######################################################################
# Government
######################################################################

building_requirement_tribal = {
	scope:holder = {
		has_government = tribal_government
	}
}

building_requirement_tribal_holding_in_county = {
	county = {
		any_county_province = {
			has_holding_type = tribal_holding
		}
	}
}

######################################################################
# Holding Level
######################################################################

building_requirement_castle_city_church = {
	trigger_if = {
		limit = { has_holding_type = castle_holding }
		has_building_or_higher = castle_$LEVEL$
	}
	trigger_else_if = {
		limit = { has_holding_type = city_holding }
		has_building_or_higher = city_$LEVEL$
	}
	trigger_else_if = {
		limit = { has_holding_type = church_holding }
		has_building_or_higher = temple_$LEVEL$
	}
	trigger_else = {
		custom_description = {
			text = "building_requirement_castle_city_church_failed"
			always = no
		}
	}
}

######################################################################
# Special
######################################################################

building_hall_of_heroes_requirement = {
	custom_description = {
		text = building_requirement_hall_of_heroes_failed
		has_variable = hall_of_heroes
		scope:holder = {
			always = no
		}
	}
}

building_university_requirement = {
	has_variable = university
}

######################################################################
# Terrain
######################################################################

building_common_tradeport_requirement_terrain = {
	is_coastal = yes
	#OR = {
	#	terrain = plains
	#	terrain = steppe
	#	terrain = desert
	#	terrain = drylands
	#	terrain = oasis
	#	terrain = farmlands
	#	terrain = floodplains
	#}
}

building_pastures_requirement_terrain = {
	OR = {
		terrain = oasis
		terrain = savanna
		terrain = semi_desert
		terrain = tropical_floodplain
		terrain = grassland
		terrain = shrubland
		terrain = tundra
		terrain = semi_desert_hills
		terrain = savanna_hills
		terrain = grassland_hills
		terrain = shrubland_hills
		terrain = tundra_hills
		terrain = montane_grassland
		terrain = alpine_desert
		terrain = atolls
		terrain = farmlands
		terrain = desert
		terrain = desert_hills
		AND = {
			OR = {
				terrain = savanna
				terrain = tropical_rainforest
				terrain = rainforest_hills
				terrain = savanna_hills
			}
			NOT = { geographical_region = world_innovation_elephants }
		}
	}
}

building_hunting_grounds_requirement_terrain = {
	OR = {
		terrain = savanna
		terrain = semi_desert
		terrain = tropical_rainforest
		terrain = forest
		terrain = grassland
		terrain = shrubland
		terrain = taiga
		terrain = tundra
		terrain = rainforest_hills
		terrain = semi_desert_hills
		terrain = savanna_hills
		terrain = forest_hills
		terrain = grassland_hills
		terrain = shrubland_hills
		terrain = taiga_hills
		terrain = tundra_hills
		terrain = wetlands
	}
}

building_orchards_requirement_terrain = {
	OR = {
		terrain = oasis
		terrain = tropical_floodplain
		terrain = shrubland
		terrain = savanna_hills
		terrain = shrubland_hills
	}
}

building_farm_estates_requirement_terrain = {
		terrain = farmlands
}

building_military_camps_requirement_terrain = {
	OR = {
		terrain = oasis
		terrain = savanna
		terrain = semi_desert
		terrain = tropical_rainforest
		terrain = tropical_floodplain
		terrain = forest
		terrain = grassland
		terrain = shrubland
		terrain = taiga
		terrain = tundra
		terrain = rainforest_hills
		terrain = semi_desert_hills
		terrain = savanna_hills
		terrain = forest_hills
		terrain = grassland_hills
		terrain = shrubland_hills
		terrain = taiga_hills
		terrain = tundra_hills
		terrain = montane_rainforest
		terrain = montane_forest
		terrain = montane_grassland
		terrain = montane_taiga
		terrain = alpine_desert
		terrain = atolls
		terrain = farmlands
		terrain = wetlands
		terrain = desert
		terrain = desert_hills
	}
}

building_regimental_grounds_requirement_terrain = {
	OR = {
		terrain = tropical_floodplain
		terrain = farmlands
	}
}

building_ramparts_requirement_terrain = {
	OR = {
		terrain = forest
		terrain = taiga
	}
}

building_curtain_walls_requirement_terrain = {
	OR = {
		terrain = savanna
		terrain = grassland
		terrain = shrubland
		terrain = farmlands
		terrain = wetlands
	}
}

building_watchtowers_requirement_terrain = {
	OR = {
		terrain = oasis
		terrain = semi_desert
		terrain = tropical_rainforest
		terrain = tropical_floodplain
		terrain = tundra
		terrain = atolls
		terrain = desert
	}
}

building_cereal_fields_requirement_terrain = {
	OR = {
		terrain = savanna
		terrain = semi_desert
		terrain = tropical_floodplain
		terrain = grassland
		terrain = shrubland
		terrain = farmlands
	}
}

building_outposts_requirement_terrain = {
	OR = {
		terrain = savanna
		terrain = semi_desert
		terrain = tropical_rainforest
		terrain = forest
		terrain = taiga
		terrain = tundra
		terrain = rainforest_hills
		terrain = semi_desert_hills
		terrain = savanna_hills
		terrain = forest_hills
		terrain = grassland_hills
		terrain = shrubland_hills
		terrain = taiga_hills
		terrain = tundra_hills
		terrain = montane_rainforest
		terrain = montane_forest
		terrain = montane_grassland
		terrain = montane_taiga
		terrain = alpine_desert
		terrain = atolls
		terrain = wetlands
		terrain = desert
		terrain = desert_hills
	}
}

building_barracks_requirement_terrain = {
	OR = {
		terrain = savanna
		terrain = semi_desert
		terrain = tropical_rainforest
		terrain = tropical_floodplain
		terrain = forest
		terrain = grassland
		terrain = shrubland
		terrain = taiga
		terrain = rainforest_hills
		terrain = semi_desert_hills
		terrain = savanna_hills
		terrain = forest_hills
		terrain = grassland_hills
		terrain = shrubland_hills
		terrain = taiga_hills
		terrain = montane_forest
		terrain = montane_grassland
		terrain = montane_taiga
		terrain = farmlands
		terrain = wetlands
	}
}

building_camel_farms_requirement_terrain = {
	OR = {
		terrain = oasis
		terrain = semi_desert
		terrain = tropical_floodplain
		terrain = semi_desert_hills
		terrain = alpine_desert
		terrain = desert
		terrain = desert_hills
	}
}

building_logging_camps_requirement_terrain = {
	OR = {
		terrain = tropical_rainforest
		terrain = forest
		terrain = taiga
		terrain = rainforest_hills
		terrain = forest_hills
		terrain = taiga_hills
		terrain = montane_rainforest
		terrain = montane_forest
		terrain = montane_taiga
	}
}

building_peat_quarries_requirement_terrain = {
	terrain = wetlands
}

building_hill_farms_requirement_terrain = {
	OR = {	
		terrain = semi_desert_hills
		terrain = savanna_hills
		terrain = grassland_hills
		terrain = shrubland_hills
	}
}

building_elephant_pens_requirement_terrain = {
	OR = {	
		terrain = savanna
		terrain = tropical_rainforest
		terrain = rainforest_hills
		terrain = savanna_hills
	}
	geographical_region = world_innovation_elephants
}

building_plantations_requirement_terrain = {
	OR = {
		terrain = oasis
		terrain = semi_desert
		terrain = semi_desert_hills
		terrain = desert
		terrain = desert_hills
	}
}

building_quarries_requirement_terrain = {
	OR = {
		terrain = montane_rainforest
		terrain = montane_forest
		terrain = montane_grassland
		terrain = montane_taiga
		terrain = alpine_desert
	}
}

building_hill_forts_requirement_terrain = {
	OR = {
		terrain = rainforest_hills
		terrain = semi_desert_hills
		terrain = savanna_hills
		terrain = forest_hills
		terrain = grassland_hills
		terrain = shrubland_hills
		terrain = taiga_hills
		terrain = tundra_hills
		terrain = montane_rainforest
		terrain = montane_forest
		terrain = montane_grassland
		terrain = montane_taiga
		terrain = alpine_desert
		terrain = desert_hills
	}
}

######################################################################
# Baronies that cannot have a holding (intended for Sahara)
######################################################################

barony_cannot_construct_holding = {
	#Can add effectively uninhabitables here
	OR = {
		barony = title:b_4773
		barony = title:b_4769
		barony = title:b_4768
		barony = title:b_4766
		barony = title:b_4722
		barony = title:b_4778
		barony = title:b_4777
		barony = title:b_4780
		barony = title:b_4782
		barony = title:b_4775
		barony = title:b_4784
		barony = title:b_4785
		barony = title:b_4774
		barony = title:b_4756
		barony = title:b_4757
		barony = title:b_4787
		barony = title:b_4791
		barony = title:b_4958
		barony = title:b_4956
		barony = title:b_4959
		barony = title:b_4004
		barony = title:b_4430
		barony = title:b_4414
		barony = title:b_4013
		barony = title:b_4129
		barony = title:b_4627
		barony = title:b_3795
		barony = title:b_1540
		barony = title:b_3930
		barony = title:b_3920
	}
}
